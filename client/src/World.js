import Actor from './Actor.js'

// This represents the current state of the world, including all objects in it.
class World {
  constructor () {
    this.actors = {}
  }

  update () {
    Object.values(this.actors).forEach(actor => actor.update())
  }

  // When we receive an update from server...
  syncState (state) {
    const newActorIDs = Object.keys(state.actors)

    // Delete any actors who no longer exist:
    Object.keys(this.actors).forEach(actorID => {
      if (!newActorIDs.includes(actorID)) { delete this.actors[actorID] }
    })

    // Update (or locally-create) the ones who do:
    newActorIDs.forEach(actorID => {
      if (!this.actors[actorID]) { this.actors[actorID] = new Actor() }
      this.actors[actorID].syncState(state.actors[actorID])
    })
  }

  getActors () {
    return Object.values(this.actors)
  }
}

export default World
