// This represents any kind of "object" in the world.
class Actor {
  constructor (json) {
    this.x = undefined
    this.y = undefined
  }

  // This is called when we receive a "state update" from the server.
  syncState (json) {
    this.targetX = json.x
    this.targetY = json.y

    // If we don't know our position yet, just move us directly to the target:
    if (this.x === undefined) { this.x = json.x }
    if (this.y === undefined) { this.y = json.y }

    // See .update() below...
    this.dx = this.targetX - this.x
    this.dy = this.targetY - this.y

    // These fields are optional, and are not included on every update:
    if (json.name) { this.name = json.name }
    if (json.kind) { this.kind = json.kind }
  }

  // Slide the "local" position towards the "actual" one stated by the server:
  update () {
    // .update() is called 60 times per second; .syncState() is called 10 times
    // per second. Thus, we should ideally resolve the difference in 6 frames:
    this.x += this.dx / 6
    this.y += this.dy / 6
    // Stop trying to move towards the target if we're basically already on it:
    if (Math.abs(this.x - this.targetX) < 0.01) { this.dx = 0 }
    if (Math.abs(this.y - this.targetY) < 0.01) { this.dy = 0 }
  }
}

export default Actor
