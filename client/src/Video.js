// This class handles the actual drawing of the world (and actors) to a canvas.
class Video {
  constructor () {
    this.canvas = document.querySelector('canvas')
    this.ctx = this.canvas.getContext('2d')
    this.player = document.querySelector('#player')
    this.crate = document.querySelector('#crate')
  }

  update (world) {
    this.ctx.fillStyle = '#888'
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

    world.getActors().forEach(actor => {
      this.ctx.fillStyle = '#FFF'

      if (actor.kind === 'player' && this.player) {
        this.ctx.drawImage(this.player, actor.x, actor.y)
      } else if (actor.kind === 'crate' && this.crate) {
        this.ctx.drawImage(this.crate, actor.x, actor.y)
      } else {
        this.ctx.fillRect(actor.x, actor.y, 16, 16)
      }

      if (actor.name) {
        this.ctx.fillStyle = 'yellow'
        this.ctx.fillText(actor.name, actor.x, actor.y)
      }
    })
  }
}

export default Video
