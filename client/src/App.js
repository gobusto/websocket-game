import Keyboard from './Keyboard.js'
import Network from './Network.js'
import Video from './Video.js'
import World from './World.js'

// This is the main class, which handles general stuff like the main loop.
class App {
  constructor () {
    this.world = new World()

    this.keyboard = new Keyboard(this.onKeyEvent.bind(this))
    this.video = new Video()
    this.network = new Network('ws://0.0.0.0:1337', {
      update: this.onServerUpdate.bind(this)
    })

    this.mainLoop()
  }

  // Send input to the server ASAP; network-send time should be the only delay!
  onKeyEvent (key, state) {
    if (key === 'ArrowUp') { this.network.send({ up: state }) }
    if (key === 'ArrowDown') { this.network.send({ down: state }) }
    if (key === 'ArrowLeft') { this.network.send({ left: state }) }
    if (key === 'ArrowRight') { this.network.send({ right: state }) }
  }

  // See script.js - this is just a quick, hacked-in way of setting a name:
  onMessage (text) {
    this.network.send({ name: text })
  }

  // When the server gives us a new "actual" state, we need to try to match it:
  onServerUpdate (state) {
    this.world.syncState(state)
  }

  // Update the local simulation state and redraw the display:
  mainLoop (currentTime) {
    requestAnimationFrame(this.mainLoop.bind(this)) // Keep looping!

    if (!currentTime) { return } // Happens when called from the constructor...
    if (!this.timer) { this.timer = currentTime } // First real call; do setup.

    // We want to run at a constant 60 Hz - no more, no less - for consistency:
    let updated = false
    while (this.timer <= currentTime) {
      this.world.update()
      this.timer += 1000 / 60
      updated = true
    }

    // Don't bother re-drawing the display if nothing has actually changed:
    if (updated) { this.video.update(this.world) }
  }
}

export default App
