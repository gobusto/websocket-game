// This class encapsulates anything necessary for keyboard input to work.
class Keyboard {
  constructor (callback) {
    this.callback = callback
    document.body.onkeydown = this.handle.bind(this)
    document.body.onkeyup = this.handle.bind(this)
  }

  handle (event) {
    if (!event.repeat) { this.callback(event.key, event.type === 'keydown') }
  }
}

export default Keyboard
