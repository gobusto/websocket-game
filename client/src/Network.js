// This class simply encapsulates any websocket-related functionality.
class Network {
  constructor (url, callbacks) {
    this.socket = new WebSocket(url)

    this.socket.onopen = (event) => {
      if (callbacks.connect) { callbacks.connect() }
    }

    this.socket.onclose = (event) => {
      if (!callbacks.disconnect) { return }
      callbacks.disconnect(event.code, event.reason)
    }

    this.socket.onmessage = (event) => {
      const json = JSON.parse(event.data)
      // console.log('RECEIVE:', event.data)
      if (callbacks.update) { callbacks.update(json) }
    }

    this.socket.onerror = (event) => console.log('Error:', event)
  }

  send (data) {
    // console.log('SEND:', data)
    this.socket.send(JSON.stringify(data))
  }
}

export default Network
