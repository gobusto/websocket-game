import App from './src/App.js'

window.onload = function () {
  'use strict'

  const app = new App()

  document.querySelector('button').onclick = () => {
    app.onMessage(document.querySelector('input').value)
  }
}
