# frozen_string_literal: true

require_relative './actors/player.rb'

# This class represents a generic object within the game world.
class Actor
  attr_reader :user # Readable so that our "owner" knows to give us messages.
  attr_reader :kind # Useful for deciding various things in scripts.
  attr_accessor :position, :velocity # Editable for scripts/physics purposes.
  attr_accessor :destroy # Accessible for scripts (and "garbage collection").

  KNOWN_ACTOR_TYPES = {
    'player' => Actors::Player
  }.freeze

  def initialize(user: nil, kind: nil, at_x: 0, at_y: 0)
    @user = user
    @kind = kind || 'player'
    @position = [at_x, at_y]
    @velocity = [0, 0]
    @destroy = false

    @script = KNOWN_ACTOR_TYPES[@kind].new(self) if KNOWN_ACTOR_TYPES[@kind]
  end

  def think
    @script&.think
  end

  def message(data)
    @script&.message(data)
  end

  def collide(other)
    @script ? @script.collide(other) : true
  end

  def aabb
    @script&.aabb || [16, 16]
  end

  def as_json
    extra_json = @script&.as_json || {}
    extra_json.merge('user' => @user, 'kind' => @kind, 'position' => @position)
  end
end
