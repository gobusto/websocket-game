# frozen_string_literal: true

require_relative './base.rb'

module Actors
  # Handles logic for player-type actors.
  class Player < Base
    KEYS = %w[up down left right].freeze

    def initialize(actor)
      @actor = actor
      @name = 'Anonymous'
      @input = {}
    end

    def message(data)
      @name = data['name'].to_s if data['name']
      KEYS.each { |key| @input[key] = data[key] unless data[key].nil? }
    end

    def think
      @actor.velocity[0] = -4 if @input['left']
      @actor.velocity[0] = 4 if @input['right']
      @actor.velocity[1] = -4 if @input['up']
      @actor.velocity[1] = 4 if @input['down']
    end

    def collide(other)
      other.kind != 'player'
    end

    def as_json
      { 'name' => @name }
    end
  end
end
