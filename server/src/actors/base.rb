# frozen_string_literal: true

module Actors
  # A base class for various kinds of actor scripts.
  class Base
    def initialize(actor)
      @actor = actor
    end

    def think
      # This is called once per game loop.
    end

    def collide(_other)
      true # Assume that we want to collide with other actors (or the world).
    end

    def message(_data)
      # This is used as a generic way to receive a "message" from somewhere.
    end

    def aabb
      [16, 16] # This is used for collision-detection purposes.
    end

    def as_json
      {} # Use this to include any extra details when we send data to players.
    end
  end
end
