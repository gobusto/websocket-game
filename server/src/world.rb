# frozen_string_literal: true

require_relative './actor.rb'
require_relative './physics.rb'

# This class stores the state of the "world" and updates it as time goes by.
class World
  attr_accessor :actors

  def initialize
    @actors = [
      Actor.new(kind: 'crate', at_x: 120, at_y: 20),
      Actor.new(kind: 'crate', at_x: 220, at_y: 20),
      Actor.new(kind: 'test', at_x: 20, at_y: 220)
    ]
  end

  def update(queue)
    queue.each { |event| process(event[:from], event[:type], event[:data]) }
    @actors.each(&:think)
    Physics.new.update(actors)
    @actors.reject!(&:destroy)
  end

  private

  def process(user, kind, data)
    return @actors.append(Actor.new(user: user)) if kind == :join
    return @actors.reject! { |actor| actor.user == user } if kind == :quit

    @actors.filter { |a| a.user == user }.each { |actor| actor.message(data) }
  end
end
