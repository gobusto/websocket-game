# frozen_string_literal: true

require 'em-websocket'
require_relative './src/app.rb'

HOST = ENV['HOST'] || '0.0.0.0'
PORT = ENV['PORT'] || 1337
puts("Starting a websocket server on #{HOST}:#{PORT}")

app = App.new
EM.run do
  EM::WebSocket.run(host: HOST, port: PORT) do |socket|
    socket.onopen { app.connect(socket.object_id) { |m| socket.send(m.dup) } }
    socket.onmessage { |data| app.receive(socket.object_id, data) }
    socket.onclose { app.disconnect(socket.object_id) }
  end
end
